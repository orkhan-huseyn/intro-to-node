const { EventEmitter } = require('events');

const button = new EventEmitter();

button.on('message', function (data) {
  console.log('MESSAGE RECEVIED!', data);
});

button.emit('message', 'Salam!');
button.emit('message', 'Sagh ol!');

process.on('exit', function (code) {
  console.log('Goodbye!', code);
});

throw new Error('Vay dedem vay!');
