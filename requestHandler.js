// const fs = require('fs');

const resources = {
  news: [
    {
      id: 0,
      title: 'News number 0',
      content: 'News number 0 content',
    },
    {
      id: 1,
      title: 'News number 1',
      content: 'News number 1 content',
    },
    {
      id: 2,
      title: 'News number 2',
      content: 'News number 2 content',
    },
  ],
  users: [
    {
      id: 0,
      username: '@user#0',
      fullName: 'Elekber',
    },
    {
      id: 1,
      username: '@user#1',
      fullName: 'Allahshukur',
    },
  ],
};

function requestHandler(req, res) {
  const [, resource, id] = req.url.split('/');

  if (req.url === 'GET') {
    // do some resource
  } else if (req.url === 'POST') {
    // 
  }

  if (!(resource in resources)) {
    res.writeHead(404, {
      'Content-Type': 'application/json',
    });
    res.end(
      JSON.stringify({
        message: 'Resource not found',
      })
    );
  }

  if (id) {
    const response = resources[resource][id];
    if (!response) {
      res.writeHead(404, {
        'Content-Type': 'application/json',
      });
      res.end(
        JSON.stringify({
          message: 'News not found',
        })
      );
    } else {
      res.writeHead(200, {
        'Content-Type': 'application/json',
      });
      res.end(JSON.stringify(response));
    }

    return;
  }

  res.writeHead(200, {
    'Content-Type': 'application/json',
  });
  res.end(JSON.stringify(news));
  // switch (req.url) {
  //   case '/':
  //     fs.readFile('pages/index.html', function (err, contents) {
  //       res.end(contents.toString());
  //     });
  //     break;
  //   case '/about':
  //     fs.readFile('pages/about.html', function (err, contents) {
  //       res.end(contents.toString());
  //     });
  //     break;
  //   case '/news/1':
  //     break;
  //   case '/news':
  //     if (req.method === 'GET') {
  //       fs.readFile('pages/news.html', function (err, contents) {
  //         res.end(contents.toString());
  //       });
  //       return;
  //     }

  //     if (req.method === 'POST') {
  //       req.on('data', function (data) {
  //         const post = JSON.parse(data.toString());
  //         news.push(post);
  //       });
  //       res.writeHead(201, {
  //         'Content-Type': 'application/json',
  //       });
  //       res.end(JSON.stringify(news));
  //       return;
  //     }

  //     break;
  //   default:
  //     res.writeHead(404, {
  //       'Content-Type': 'text/html',
  //       'Set-Cookie': 'qwerty=219ffwef9w0f;',
  //     });
  //     res.end('<h1>Not Found!</h1>');
  // }
}

module.exports = requestHandler;
