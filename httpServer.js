const http = require('http');
const requestHandler = require('./requestHandler');

const server = http.createServer();

server.on('request', requestHandler);

server.listen(8080, function () {
  console.log('Server started and running on port 8080!');
});
